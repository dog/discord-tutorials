import discord
from discord.ext import commands

bot = commands.Bot(command_prefix="?")

@bot.command()
async def ping(ctx):
    await ctx.send("Pong! :D")

@bot.command()
async def embed(ctx):
    embed = discord.Embed(title="My Embed!", description="This is my first embed! :)", color=0xB0E0E6)
    embed.add_field(name="Field 1", value="This is a field!", inline=False)
    await ctx.send(embed=embed)
bot.run("TOKEN HERE")